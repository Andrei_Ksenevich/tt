package testbase;

import core.DriverProvider;
import lombok.extern.slf4j.Slf4j;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.Reporter;

//Custom listener for testNG used instead of test->abstractTest with before and after annotations
@Slf4j
public class CustomTestSuiteListener implements ISuiteListener {

    public void onStart(ISuite iSuite) {
        String url = iSuite.getParameter("Url");
        DriverProvider.getDriver().get(url);
        Reporter.log("Opening Url: "+url);
        log.info("Opening Url: "+url);
    }

    public void onFinish(ISuite iSuite) {
        Reporter.log("Closing browser.");
        log.info("Closing browser.");
        DriverProvider.tearDown();
    }
}
