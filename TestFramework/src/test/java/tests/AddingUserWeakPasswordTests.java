package tests;

import bo.User;
import core.InjectionsModule;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;
import po.LoginPage;

import javax.inject.Inject;

@Slf4j
@Guice(modules = { InjectionsModule.class })
public class AddingUserWeakPasswordTests {

    private static final User USER_WEAK_PASSWORD = new User("user1login","user1fullname","");
    private static final String[] WEAK_PASSWORDS = {"12345","password","12345λ","passwordλ"};

    @Inject
    private LoginPage loginPage;

    @Test(description = "Testing user with weak password can not be added.")
    public void testAddUserWeakPassword(){
        Reporter.log("Testing user with weak password can not be added.");
        int currentAddedUsersAmount = loginPage.getAddedUsers();
        for(String weakPassword: WEAK_PASSWORDS){
            USER_WEAK_PASSWORD.setPassword(weakPassword);
            loginPage.logIn(USER_WEAK_PASSWORD);
            Reporter.log("Trying to register user with the following weak password: "+ USER_WEAK_PASSWORD.getPassword());
            log.info("Trying to register user with the following weak password: "+ USER_WEAK_PASSWORD.getPassword());
            log.info(loginPage.getStatusText());
            Assert.assertEquals(loginPage.getStatusText(), "Status: Password does not conform rules");
            Assert.assertEquals(loginPage.getAddedUsers(),currentAddedUsersAmount);
            Assert.assertFalse(loginPage.isUserAdded(USER_WEAK_PASSWORD));
        }
    }
}
