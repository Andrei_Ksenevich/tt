package tests;

import bo.User;
import core.InjectionsModule;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;
import po.LoginPage;

import javax.inject.Inject;

@Slf4j
@Guice(modules = { InjectionsModule.class })
public class AddingUserTests{

    private static final User USER = new User("user1login","user1fullname","user1Password");

    @Inject
    private LoginPage loginPage;

    @Test(description = "Testing adding of a valid user.")
    public void testAddUser(){
        Reporter.log("Testing adding of a valid user.");
        log.info("Testing adding of a valid user.");
        int currentAddedUsersAmount = loginPage.getAddedUsers();
        loginPage.logIn(USER);
        log.info(loginPage.getStatusText());
        Assert.assertEquals(loginPage.getStatusText(), "Status: user "+USER.getUserLogInName()+" was created");
        Assert.assertEquals(loginPage.getAddedUsers(),currentAddedUsersAmount+1);
        Assert.assertTrue(loginPage.isUserAdded(USER));
    }
}
