package tests;

import bo.User;
import core.InjectionsModule;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;
import po.LoginPage;

import javax.inject.Inject;

@Slf4j
@Guice(modules = { InjectionsModule.class })
public class AddingUserEmptyFieldsTests {

    private static final User USER_EMPTY_LOGIN = new User("","user1fullname","user1Password");
    private static final User USER_EMPTY_FULL_NAME = new User("user1login","","user1Password");
    private static final User USER_EMPTY_PASSWORD = new User("user1login","user1fullname","");

    @Inject
    private LoginPage loginPage;

    @Test(description = "Testing user with empty login can not be added.")
    public void testAddUserEmptyLogin(){
        Reporter.log("Testing user with empty login can not be added.");
        log.info("Testing user with empty login can not be added.");
        int currentAddedUsersAmount = loginPage.getAddedUsers();
        loginPage.logIn(USER_EMPTY_LOGIN);
        log.info(loginPage.getStatusText());
        Assert.assertEquals(loginPage.getStatusText(), "Status: Login cannot be empty");
        Assert.assertEquals(loginPage.getAddedUsers(),currentAddedUsersAmount);
        Assert.assertFalse(loginPage.isUserAdded(USER_EMPTY_LOGIN));
    }

    @Test(description = "Testing user with empty full name can not be added.")
    public void testAddUserEmptyFullName(){
        Reporter.log("Testing user with empty full name can not be added.");
        int currentAddedUsersAmount = loginPage.getAddedUsers();
        loginPage.logIn(USER_EMPTY_FULL_NAME);
        log.info(loginPage.getStatusText());
        Assert.assertEquals(loginPage.getStatusText(), "Status: Full name cannot be empty");
        Assert.assertEquals(loginPage.getAddedUsers(),currentAddedUsersAmount);
        Assert.assertFalse(loginPage.isUserAdded(USER_EMPTY_FULL_NAME));
    }

    @Test(description = "Testing user with empty password can not be added.")
    public void testAddUserEmptyPassword(){
        Reporter.log("Testing user with empty password can not be added.");
        int currentAddedUsersAmount = loginPage.getAddedUsers();
        loginPage.logIn(USER_EMPTY_PASSWORD);
        log.info(loginPage.getStatusText());
        Assert.assertEquals(loginPage.getStatusText(), "Status: Password cannot be empty");
        Assert.assertEquals(loginPage.getAddedUsers(),currentAddedUsersAmount);
        Assert.assertFalse(loginPage.isUserAdded(USER_EMPTY_PASSWORD));
    }
}
