package bo;

//class which represents user to add into a system
public class User {
    private String userLogInName;
    private String fullUserName;
    private String password;

    public User(String userLogInName, String fullUserName, String password) {
        this.userLogInName = userLogInName;
        this.fullUserName = fullUserName;
        this.password = password;
    }


    public String getUserLogInName() {
        return userLogInName;
    }

    public void setUserLogInName(String userLogInName) {
        this.userLogInName = userLogInName;
    }

    public String getFullUserName() {
        return fullUserName;
    }

    public void setFullUserName(String fullUserName) {
        this.fullUserName = fullUserName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
