package po;

import core.DriverProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public abstract class AbstractPage {

    private WebDriver driver = DriverProvider.getDriver();

    public AbstractPage(){
        PageFactory.initElements(this.driver, this);
    }


}
