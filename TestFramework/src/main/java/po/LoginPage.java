package po;

import bo.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class LoginPage extends AbstractPage {

    @FindBy(id = "username")
    private WebElement userNameInputfiled;

    @FindBy(id = "name")
    private WebElement fullNameInputfiled;

    @FindBy(id = "password")
    private WebElement passwordInputfiled;

    @FindBy(id = "submit")
    private WebElement logInButton;

    @FindBy(id = "status")
    private WebElement status;

    @FindBy(id = "users")
    private List<WebElement> addedUsers;


    public LoginPage() {
        super();
    }

    public void logIn(User user){
        userNameInputfiled.sendKeys(user.getUserLogInName());

        fullNameInputfiled.sendKeys(user.getFullUserName());

        passwordInputfiled.sendKeys(user.getPassword());

        logInButton.click();
    }

    public String getStatusText(){
        return status.getText();
    }

    public int getAddedUsers(){
        return addedUsers.size();
    }

    public boolean isUserAdded(User user){
        for (WebElement addedUser: addedUsers) {
            if(addedUser.getText().equals("Name: "+user.getFullUserName()+" login: "+user.getUserLogInName()+" password "+user.getPassword())){
                return true;
            }
        }
        return false;
    }


}
