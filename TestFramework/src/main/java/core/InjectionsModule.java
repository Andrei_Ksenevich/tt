package core;

import com.google.inject.AbstractModule;
import po.LoginPage;

public class InjectionsModule extends AbstractModule {

    //bindings for guice injections
    @Override
    public void configure(){
        bind(LoginPage.class);
    }
}
