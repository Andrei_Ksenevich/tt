package core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class DriverProvider {


    private static WebDriver driver;

    public static WebDriver getDriver() {
        if (driver == null) {
            //hardcode for now with chromedriver is in system properies, should be replaced
            //with some kind of DriverFactory which will provide required driver based on some properties or other configs(xm; e.t.c).
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            driver = new ChromeDriver(capabilities);
        }
        return driver;
    }

    public static void tearDown() {
        if (driver!=null) {
            driver.quit();
        }
    }
}
