package business.service;

import org.junit.Test;
import peristance.IUserRepository;
import peristance.InMemoryUserRepository;

import static org.junit.Assert.*;

public class UserServiceTest {

    private  UserService userService = new UserService();
    private  IUserRepository repository = new InMemoryUserRepository();


    @Test
    public void testUserServiceAddUser(){
        userService.setUserRepository(repository);
        assertEquals("user userA was created",userService.addUser("userA","userFullA","1password"));
        assertEquals("Name: userFullA login: userA password 1password",userService.getUserInfoList().get(0));
    }

    @Test
    public void testUserServiceAddUserEmptyLogin(){
        userService.setUserRepository(repository);
        assertEquals("Login cannot be empty",userService.addUser("","userFullA","1password"));
        assertEquals(0,userService.getUserInfoList().size());
    }

    @Test
    public void testUserServiceAddUserEmptyFullname(){
        userService.setUserRepository(repository);
        assertEquals("Full name cannot be empty",userService.addUser("userA","","1password"));
        assertEquals(0,userService.getUserInfoList().size());
    }

    @Test
    public void testUserServiceAddUserEmptyPassword(){
        userService.setUserRepository(repository);
        assertEquals("Password cannot be empty",userService.addUser("userA","userFullA",""));
        assertEquals(0,userService.getUserInfoList().size());
    }

    @Test
    public void testUserServiceAddUserWeakPassword(){
        userService.setUserRepository(repository);
        assertEquals("Password does not conform rules",userService.addUser("userA","userFullA","1"));
        assertEquals("Password does not conform rules",userService.addUser("userA","userFullA","password"));
        assertEquals("Password does not conform rules",userService.addUser("userA","userFullA","1λ"));
        assertEquals(0,userService.getUserInfoList().size());
    }

}