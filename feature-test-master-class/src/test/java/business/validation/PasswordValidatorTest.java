package business.validation;

import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

public class PasswordValidatorTest {
    private  static final String VALID_PASSWORD = "1password";
    private  static final String INVALID_PASSWORD_NUMBERS = "1";
    private  static final String INVALID_PASSWORD_LETTERS = "password";
    private  static final String INVALID_PASSWORD_NOT_ALPHA = "1λ";
    private  static final String EMPTY_INPUT = "";
    private  IFieldValidator passwordValidator = new PasswordValidator();

    @Test
    public void getFileNamePassword() {
        assertEquals("Password",passwordValidator.getFieldName());
    }

    @Test
    public void validatePassword() {
        assertFalse(passwordValidator.validate(VALID_PASSWORD).isPresent());
    }

    @Test
    public void validatePasswordEmpty() {
        assertEquals("Password cannot be empty",passwordValidator.validate(EMPTY_INPUT).get().getErrorMessage());
    }

    @Test
    public void validatePasswordWeak() {
        try {
            assertEquals("Password does not conform rules", passwordValidator.validate(INVALID_PASSWORD_NUMBERS).get().getErrorMessage());
            assertEquals("Password does not conform rules", passwordValidator.validate(INVALID_PASSWORD_LETTERS).get().getErrorMessage());
            assertEquals("Password does not conform rules", passwordValidator.validate(INVALID_PASSWORD_NOT_ALPHA).get().getErrorMessage());
        } catch(NoSuchElementException e) {
            fail("Invalid password was validated.");
        }

    }
}
