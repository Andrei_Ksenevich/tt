package business.validation;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class FullUserNameValidatorTest {
    private  static final String VALID_FULLNAME = "fullname";
    private  static final String EMPTY_INPUT = "";
    private  IFieldValidator fullUserNameValidator = new FullUserNameValidator();

    @Test
    public void getFieldNameFullUserName() {
        assertEquals("Full name",fullUserNameValidator.getFieldName());
    }

    @Test
    public void validateFullName() {
        assertFalse(fullUserNameValidator.validate(VALID_FULLNAME).isPresent());
    }

    @Test
    public void validateFullNameEmpty() {
        assertEquals("Full name cannot be empty",fullUserNameValidator.validate(EMPTY_INPUT).get().getErrorMessage());
    }
}
