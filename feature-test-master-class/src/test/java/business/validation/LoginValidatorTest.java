package business.validation;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class LoginValidatorTest {
    private  static final String VALID_LOGIN = "login";
    private  IFieldValidator loginValidator = new LoginValidator();

    private  static final String EMPTY_INPUT = "";


    @Test
    public void getFieldNameLogin() {
        assertEquals("Login",loginValidator .getFieldName());
    }

    @Test
    public void validateLogin() {
        assertFalse(loginValidator.validate(VALID_LOGIN).isPresent());
    }

    @Test
    public void validateLoginEmpty() {
        assertEquals("Login cannot be empty",loginValidator.validate(EMPTY_INPUT).get().getErrorMessage());
    }
}
